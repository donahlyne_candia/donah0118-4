import os
import sys

# basic Fabric settings
FAB_HOSTS = ['178.62.91.111']# 206.189.79.34'] # development server
FAB_USER = 'root'
FAB_PASSWORD = 'lifeisawesome'
FAB_FORWARD_AGENT = True

# other configuration data that is different from stage to stage
FAB_DOMAIN = "web-template.beta-wordbean.info"
FAB_PROJECT_NAME = "Development"
FAB_DEPLOY_PATH = '/usr/local/project'
FAB_REPO_URL = 'https://bitbucket.org/wordbean/internal_templates.git'
FAB_REPO_BRANCH = 'LS-LS-3210_bitbucket_pipeline'
FAB_LINKED_DIRS = ['static']

# MYSQL_USER = 'root'
# MYSQL_PASSWORD = 'some good password (REPLACE!!)'

