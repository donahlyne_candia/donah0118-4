import os
import sys

# basic Fabric settings
FAB_HOSTS = ['some.ip'] # production server
FAB_USER = 'root'
FAB_PASSWORD = 'some good password (REPLACE!!)'
FAB_FORWARD_AGENT = True

# other configuration data that is different from stage to stage
FAB_DOMAIN = ""
FAB_PROJECT_NAME = "Production Stage"
FAB_DEPLOY_PATH = '/home/deployer/app'
FAB_REPO_URL = 'git@github.com:user/repo.git'
FAB_REPO_BRANCH = 'master'
FAB_LINKED_DIRS = ['static']

MYSQL_USER = 'root'
MYSQL_PASSWORD = 'some good password (REPLACE!!)'

