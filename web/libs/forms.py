import flask_admin
from flask_admin.contrib.fileadmin import FileAdmin
from flask_wtf import Form
from wtforms import TextAreaField
from wtforms.validators import DataRequired, Length
from validations import ensure_valid_url


class UploadPool(FileAdmin):
    required_filename = None

    def _save_form_files(self, directory, path, form):
        filename = self._separator.join([directory, form.upload.data.filename])

        if self.storage.path_exists(filename):
            raise Exception('File already exists. Please delete existing file if needs to be updated.')
        elif self.required_filename and self.required_filename != form.upload.data.filename:
            raise Exception('Required file name "%s".' % self.required_filename)
        else:
            self.save_file(filename, form.upload.data)
            self.on_file_upload(directory, path, filename)

class ScraperForm(Form):
    url = TextAreaField("Starting url to scrape",
                       [DataRequired(), Length(3, 8192), ensure_valid_url]
                       )