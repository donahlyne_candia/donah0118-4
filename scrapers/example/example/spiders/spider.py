# -*- coding: utf-8 -*-
import os
import scrapy
from scrapy.http import Request
import pandas as pd
from scrapy.loader import ItemLoader
from scrapy.spiders import CrawlSpider

from ..items import ExampleItem


def csv_file(file):
    return file.endswith('.csv') or file.endswith('.jl')


def excel_file(file):
    return file.endswith('.xls') or file.endswith('.xlsx')


class ExampleSpider(CrawlSpider):
    name = "spider_name"
    start_urls = ()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        from_crawler = super(ExampleSpider, cls).from_crawler
        spider = from_crawler(crawler, *args, **kwargs)
        return spider

    def __init__(self,
                 files_dir="/var/www/web_ui/working_directories/upload",
                 column_name='src',
                 *args, **kwargs):
        super(ExampleSpider, self).__init__(*args, **kwargs)
        self.urls = []

        os.chdir(files_dir)
        self.read_oldest_file(files_dir, column_name)
        self._append_to_start_urls()

    def _append_to_start_urls(self):
        self.start_urls = set(self.urls)

    def read_oldest_file(self, files_dir, column_name):
        oldest_file = min(os.listdir(files_dir), key=os.path.getmtime)
        if csv_file(oldest_file):
            df = pd.read_csv(oldest_file, encoding="utf-8", dtype=object)
            df.columns = [column_name]
            self.urls = list(df[column_name])
            self.total_input = df[column_name].count()

        elif excel_file(oldest_file):
            df = pd.read_excel(oldest_file, encoding="utf-8", dtype=object)
            df.columns = [column_name]
            self.urls = list(df[column_name])
            self.total_input = df[column_name].count()

    def parse(self, response):
        item = ItemLoader(ExampleItem())

        item.add_value('src', response.url)
        item.add_value('body', str(len(response.body)))

        return item.load_item()
