#!/usr/bin/python
# -*-coding:utf-8-*-

import scrapy
from scrapy import signals
import logging
import os
import json
import itertools


class LogScrapyMiddleware(object):
    estimated_urls_per_start_url = 50
    total_input = 0
    scraped_lines = set()

    def __init__(self, settings):
        self.settings = settings
        self.start_request_len = 0

    @classmethod
    def from_crawler(cls, crawler):
        o = cls(crawler.settings)
        crawler.signals.connect(o.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(o.spider_closed, signal=signals.spider_closed)
        return o

    def spider_opened(self, spider):
        self.log_progress(spider, status='running')

        it1, it2 = itertools.tee(spider.crawler.engine.slot.start_requests, 2)
        spider.crawler.engine.slot.start_requests = it1
        self.start_request_len = len(list(it2))

    def process_spider_output(self, response, result, spider):
        if response.url not in self.scraped_lines:
            self.scraped_lines.add(response.url)

        self.log_progress(spider)

        for x in result:
            yield x

    def log_progress(self, spider, status='running'):
        data = {
            "number_of_scraped_items": spider.crawler.stats.get_value('item_scraped_count', 0),
            "number_of_start_urls": self.start_request_len,
            "number_of_scraped_pages": len(self.scraped_lines),
            "scraper_status": status
        }
        data['percentage'] = self._get_percentage(data)

        try:
            if not os.path.exists(self.settings['WORKING_DIRECTORY_LOGS']):
                os.makedirs(self.settings['WORKING_DIRECTORY_LOGS'])

            data = json.dumps(data)
            logging.info(
                "[ProgressStats] writing out progress %s to %s" % (str(data), self.settings['WORKING_DIRECTORY_LOGS']))
            with open(os.path.join(self.settings['WORKING_DIRECTORY_LOGS'], 'scraping_logs.json'), 'w+') as f:
                f.write(data)
        except:
            logging.info(
                "[ProgressStats] error writing data %s to %s" % (str(data), self.settings['WORKING_DIRECTORY_LOGS']))

    def _get_percentage(self, data):
        compute = 0
        divisor = self.estimated_urls_per_start_url * data.get('number_of_start_urls', 1)
        if divisor > 1:
            compute = float(data.get('number_of_scraped_pages', 0)) / divisor
        if not compute > 1:
            return int(compute * 100)
        return "Please wait..."

    def spider_closed(self, spider):
        self.log_progress(spider, status='finished')
