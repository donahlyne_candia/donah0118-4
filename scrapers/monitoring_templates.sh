# pages per minute

tail -n 100000 /var/log/web/*.log | grep "`date  -d '-1 minute' '+%Y-%m-%d %H:%M'`" | grep -o " pages (at \([0123456789]\+\) pages" | grep -o "[0123456789]\+" | awk '{s+=$1} END {printf "%.0f", s}'


# Items per minute
tail -n 100000 /var/log/web/*.log | grep "`date -d '-1 minute' '+%Y-%m-%d %H:%M'`" | grep -o " items (at \([0123456789]\+\) items" | grep -o "[0123456789]\+" | awk '{s+=$1} END {printf "%.0f", s}'



# errors

tail -n 100000 /var/log/web/*.log | grep "`date -d '-1 minute' '+%Y-%m-%d %H:%M'`" | grep ERROR | wc -l