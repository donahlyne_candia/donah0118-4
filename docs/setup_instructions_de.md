# Installationsanleitung #
*(für Linux / Ubuntu)*

## Systemanforderungen: ##
- Python 2.7

Diese Anleitung wurde mit Ubuntu 16.04 LTS getestet und funktioniert wahrscheinlich auf allen aktuellen Debian/Ubuntu 
basierten Distributionen. Die Scripte können prinzipiell auch auf anderen Linux-Distributionen, Mac und Windows laufen, jedoch 
weicht die Installation von den u.g. Schritten ab. 

Um dieses komplexere Project auf einen odere mehrere Server auszurollen wird das fabric Framework verwendet. Es wird von einem Arbeits-PC aus gestartet und führt von dort aus alle notwendigen Installationsschritte auf den Servern aus.

### Installationsablauf (lokaler Arbeits-PC) ###
1. Potentielle Probleme mit "locale" auflösen (wenn von einem nicht-englishen System eingeloggt wird):

	> `sudo apt-get install language-pack-id`

	> `sudo dpkg-reconfigure locales`

2. In das Verzeichnis des Projekts wechseln (wo diese Datei liegt)

3. Installationsscript starten, bei Bedarf die angefragten Eingaben tätigen:

	> `./install_fabric.sh`

### Installationsablauf (auf die Zielserver, mittels Fabric) ###

1. In das Verzeichnis des Projekts wechseln (wo diese Datei liegt)

2. Die Einstellungen in der Stage-Defintionsdatei `deploy/stages/production.py` anpassen (entsprechend der dort hinterlegten Kommentare)

Besonders wichtig ist, in `production.py` die verwendeten Serveradressen einzutragen und sicherzustellen, dass:

* auf die Server mittels SSH eingeloggt werden kann, entweder indem Nutzer und Password eingetragen werden oder mittels SSH-Keys Zugriff möglich ist
* der verwendete SSH-Account entweder über sudo Rechte verfügt oder selber root ist

3. die Installation starten mittels ```fab production deploy```

Sobald die Installation ohne Fehler durchgelaufe ist, ist das System einsatzbereit